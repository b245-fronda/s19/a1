// console.log("Hello World");

let username;
let password;
let role;

function login(username, password, role){
	username = prompt("Input username:");
	password = prompt("Input password:");
	role = prompt("Input role:").toLowerCase();
	if(username === ""){
		alert("Input should not be empty");
	}
	else if(password === ""){
		alert("Input should not be empty");
	}
	else if(role === ""){
		alert("Input should not be empty");
	}
	else{
		switch(role){
			case "admin":
				console.log("Welcome back to the class portal, admin!");
				break;
			case "teacher":
				console.log("Thank you for logging in, teacher!");
				break;
			case "student":
				console.log("Welcome to the class portal, student!");
				break;
			default:
				console.log("Role out of range.");
				break;
		}
	}
}

login();

function checkAverage(num1, num2, num3, num4){
	let average = (num1 + num2 + num3 + num4) / 4;
	average = Math.round(average);
	if(average <= 74){
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is F");
	}
	else if(average <= 79){
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is D");
	}
	else if(average <= 84){
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is C");
	}
	else if(average <= 89){
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is B");
	}
	else if(average <= 95){
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is A");
	}
	else if(average >= 96){
		console.log("Hello, student, your average is: " + average + ". The letter equivalent is A+");
	}
}

checkAverage(91, 93, 94, 97);